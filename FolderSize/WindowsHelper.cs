﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
namespace FolderSize
{
    class WindowsHelper
    {
        static string regPath = @"Directory\shell\Show Size\command";
        static string regPathParent = @"Directory\shell\Show Size";

        public static void SetContextMenu(bool add)
        {
            RegistryKey key = Registry.ClassesRoot;
            if (add)
            {
                var keyValue= key.OpenSubKey(regPath, false);
                if (keyValue != null)
                {
                    key.DeleteSubKey(regPath, true);
                }
                RegistryKey software = key.CreateSubKey(regPath);
                software.SetValue("", SystemVar.CurrentExePath + " \"%1\"");
                software.Close();
            }
            else
            {
                key.DeleteSubKey(regPath, false);
                key.DeleteSubKey(regPathParent, false);
                key.Close();
            }
        }
        public static bool GetContextMenu()
        {
            RegistryKey Key = Registry.ClassesRoot;
            RegistryKey cmd=Key.OpenSubKey(regPath);
            if (cmd == null)
                return false;
            object setValObj = cmd.GetValue("");
            if (setValObj == null)
                return false;
            string setVal = setValObj.ToString();
            Key.Close();
            if (setVal == SystemVar.CurrentExePath + " \"%1\"")
                return true;
            else
                return false;
        }
    }
}
