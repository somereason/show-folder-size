﻿using System.IO;

namespace FolderSize
{
    class FileTreeHelper
    {
        public static void GetFileCount(ref FileTree rootFolder)
        {
            FileInfo[] f = rootFolder.FolderInfo.GetFiles();
            rootFolder.FileCountCurrent = f.Length;
            rootFolder.FileCountWithChild += f.Length;
            for (int i = 0; i < f.Length; i++)
            {
                FileTree fileNode = new FileTree(f[i].FullName);
                fileNode.SizeCurrent = f[i].Length;
                fileNode.SizeWithChild = f[i].Length;
                rootFolder.AddChild(fileNode);

                rootFolder.SizeCurrent += f[i].Length;
            }
            rootFolder.SizeWithChild += rootFolder.SizeCurrent;

            DirectoryInfo[] d = rootFolder.FolderInfo.GetDirectories();
            rootFolder.FolderCountCurrent = d.Length;
            rootFolder.FolderCountWithChild += d.Length;
            FileTree[] subFolders = new FileTree[d.Length];
            for (int i = 0; i < d.Length; i++)
            {
                subFolders[i] = new FileTree(d[i].FullName);
                rootFolder.AddChild(subFolders[i]);
                GetFileCount(ref subFolders[i]);

                rootFolder.FileCountWithChild += subFolders[i].FileCountWithChild;
                rootFolder.SizeWithChild += subFolders[i].SizeWithChild;
                rootFolder.FolderCountWithChild += subFolders[i].FolderCountWithChild;
            }
        }
    }
}
