﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace FolderSize
{
    internal class FileTree : STreeNode
    {
        public FileTree(string physicPath)
        {
            PhysicPath = physicPath;
        }
        private string _PhysicPath;

        public long FileCountCurrent { get; set; }
        public long FolderCountCurrent { get; set; }
        public long SizeCurrent { get; set; }
        public long SizeWithChild { get; set; }
        public long FileCountWithChild { get; set; }
        public long FolderCountWithChild { get; set; }
        public DirectoryInfo FolderInfo { get; private set; }
        public string PhysicPath
        {
            get { return _PhysicPath; }
            set
            {
                _PhysicPath = value;
                FolderInfo = new DirectoryInfo(_PhysicPath);
            }
        }
        public override string ToString()
        {
            return Path.GetFileName(this.PhysicPath);
        }
    }
}
